//
//  OpenCVMethods.m
//  OpencvTest
//
//  Created by realtouch's MBP 2019 on 2020/6/15.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import "OpenCVMethods.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>


@implementation ImageConverter

+ (UIColor *)grabCutFgdColor {
    return [UIColor whiteColor];
}

+ (UIColor *)grabCutBgdColor {
    return [UIColor blackColor];
}

+ (UIColor *)grabCutPrFgdColor {
    return [UIColor colorWithRed:(191/255.0) green:(191/255.0) blue:(191/255.0) alpha:1];
}

+ (UIColor *)grabCutPrBgdColor {
    return [UIColor colorWithRed:(64/255.0) green:(64/255.0) blue:(64/255.0) alpha:1];
}

+ (UIImage *)convertColor:(UIImage *)image from:(UIColor *)fromColor to:(UIColor *)toColor {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::Mat dst;
    if (mat.channels() == 1) {
        cv::cvtColor(mat, dst, cv::COLOR_GRAY2RGBA);
    }
    else if (mat.channels() == 3) {
        cv::cvtColor(mat, dst, cv::COLOR_RGB2RGBA);
    }
    else {
        dst = mat;
    }
    
    CGFloat fromA = 0, fromR = 0, fromG = 0, fromB = 0;
    [fromColor getRed:&fromR green:&fromG blue:&fromB alpha:&fromA];
    fromA *= 255.0;
    fromR *= 255.0;
    fromG *= 255.0;
    fromB *= 255.0;
    
    CGFloat toA = 0, toR = 0, toG = 0, toB = 0;
    [toColor getRed:&toR green:&toG blue:&toB alpha:&toA];
    toA *= 255.0;
    toR *= 255.0;
    toG *= 255.0;
    toB *= 255.0;
    
    cv::Mat mask;
    cv::inRange(dst,
                cv::Scalar((int)fromR, (int)fromG, (int)fromB, (int)fromA),
                cv::Scalar((int)fromR, (int)fromG, (int)fromB, (int)fromA),
                mask);
    dst.setTo(cv::Scalar((int)toR, (int)toG, (int)toB, (int)toA), mask);
    
    return MatToUIImage(dst);
}

+ (UIImage *)grayScaleImage:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::Mat gray;
    cv::cvtColor(mat, gray, cv::COLOR_RGBA2GRAY);
    
    return MatToUIImage(gray);
}

+ (UIImage *)binaryImage:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::Mat gray;
    cv::cvtColor(mat, gray, cv::COLOR_RGBA2GRAY);
    
    cv::Mat bin;
    cv::threshold(gray, bin, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    
    return MatToUIImage(bin);
}

+ (UIImage *)grabCut:(UIImage *)image foregroundBound:(CGRect)bounds {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::cvtColor(mat, mat, cv::COLOR_RGBA2RGB);
    
    cv::Rect rectangle(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height);
    
    cv::Mat mask; // segmentation (4 possible values)
    cv::Mat bgModel, fgModel; // the models (internally used)
    
    mask.setTo(cv::GC_PR_BGD);
    bgModel.setTo(0);
    fgModel.setTo(0);
    
    cv::grabCut(mat, mask, rectangle, bgModel, fgModel, 5, cv::GC_INIT_WITH_RECT);
    
    return MatToUIImage([self maskConvert:mask]);
}

+ (UIImage *)grabCut:(UIImage *)image mask:(UIImage *)maskImage {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    // 轉換顏色
    cv::cvtColor(mat, mat, cv::COLOR_RGBA2RGB);
    
    // 銳化
    mat = [self sharpnessWithMatrix:mat];
    
    cv::Rect rectangle;
    
    cv::Mat mask;
    UIImageToMat(maskImage, mask, true);
    
    mask = [self findContoursAndCreateTrimap:mask dilateSize:kDefaultDilateSize]; // segmentation (4 possible values)
    cv::Mat bgModel, fgModel; // the models (internally used)
    
    bgModel.setTo(0);
    fgModel.setTo(0);
    
    cv::grabCut(mat, mask, rectangle, bgModel, fgModel, 10, cv::GC_INIT_WITH_MASK);
    
    return MatToUIImage([self maskConvert:mask]);
}

+ (cv::Mat)maskConvert:(cv::Mat)mask {
    int cols = mask.cols;
    int rows = mask.rows;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    cvMat.setTo(0);
    
    // Foreground
    CGFloat a = 0, r = 0, g = 0, b = 0;
    UIColor *color = [self grabCutFgdColor];
    [color getRed:&r green:&g blue:&b alpha:&a];
    a *= 255;
    r *= 255;
    g *= 255;
    b *= 255;
    cv::Mat maskRange;
    cv::inRange(mask, cv::GC_FGD, cv::GC_FGD, maskRange);
    cvMat.setTo(cv::Scalar((int)r, (int)g, (int)b, (int)a), maskRange);
    
    // Background
    a = 0; r = 0; g = 0; b = 0;
    color = [self grabCutBgdColor];
    [color getRed:&r green:&g blue:&b alpha:&a];
    a *= 255;
    r *= 255;
    g *= 255;
    b *= 255;
    maskRange.setTo(0);
    cv::inRange(mask, cv::GC_BGD, cv::GC_BGD, maskRange);
    cvMat.setTo(cv::Scalar((int)r, (int)g, (int)b, (int)a), maskRange);
    
    // Possible foreground
    a = 0; r = 0; g = 0; b = 0;
    color = [self grabCutPrFgdColor];
    [color getRed:&r green:&g blue:&b alpha:&a];
    a *= 255;
    r *= 255;
    g *= 255;
    b *= 255;
    maskRange.setTo(0);
    cv::inRange(mask, cv::GC_PR_FGD, cv::GC_PR_FGD, maskRange);
    cvMat.setTo(cv::Scalar((int)r, (int)g, (int)b, (int)a), maskRange);
    
    // Possible background
    a = 0; r = 0; g = 0; b = 0;
    color = [self grabCutPrBgdColor];
    [color getRed:&r green:&g blue:&b alpha:&a];
    a *= 255;
    r *= 255;
    g *= 255;
    b *= 255;
    maskRange.setTo(0);
    cv::inRange(mask, cv::GC_PR_BGD, cv::GC_PR_BGD, maskRange);
    cvMat.setTo(cv::Scalar((int)r, (int)g, (int)b, (int)a), maskRange);
    
    /*
    uchar* data = mask.data;
    
    int fgd, bgd, pfgd, pbgd;
    fgd = 0;
    bgd = 0;
    pfgd = 0;
    pbgd = 0;
    
    for (int y = 0; y < rows; y ++) {
        for (int x = 0; x < cols; x ++) {
            int index = cols * y + x;
            if (data[index] == cv::GC_FGD) {
                cvMat.at<cv::Vec4b>(cv::Point(x,y)) = cv::Vec4b(255, 255, 255, 0);
                fgd ++;
            }
            else if (data[index] == cv::GC_BGD) {
                cvMat.at<cv::Vec4b>(cv::Point(x,y)) = cv::Vec4b(0, 0, 0, 255);
                bgd ++;
            }
            else if (data[index] == cv::GC_PR_FGD) {
                cvMat.at<cv::Vec4b>(cv::Point(x,y)) = cv::Vec4b(191, 191, 191, 80);
                pfgd ++;
            }
            else if (data[index] == cv::GC_PR_BGD) {
                cvMat.at<cv::Vec4b>(cv::Point(x,y)) = cv::Vec4b(64, 64, 64, 220);
                pbgd ++;
            }
        }
    }
    NSLog(@"fgd: %d, bgd: %d, pfgd: %d, pbgd: %d, total: %d, width*height: %d", fgd, bgd, pfgd, pbgd, fgd+bgd+pfgd+pbgd, cols*rows);
    */
     
    return cvMat;
}

+ (cv::Mat)findContoursAndCreateTrimap:(cv::Mat)binaryMat dilateSize:(int)size {
    cv::Mat mat = binaryMat;
    
    // 灰階
    cv::Mat gray;
    if (mat.channels() == 3) {
        cv::cvtColor(mat, gray, cv::COLOR_RGB2GRAY);
    }
    else if (mat.channels() == 4) {
        cv::cvtColor(mat, gray, cv::COLOR_RGBA2GRAY);
    }
    else {
        gray = mat;
    }
    
    cv::cvtColor(gray, mat, cv::COLOR_GRAY2RGBA);
    
    // 找輪廓
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(gray, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
    
    // 把輪廓畫成灰色
    cv::drawContours(mat, contours, -1, cv::Scalar(127, 127, 127, 255));
    
    // -----------------------------------
    // Gray to White
    // White to Gray
    
    cv::Mat maskGray;
    cv::inRange(mat, cv::Scalar(127, 127, 127, 255), cv::Scalar(127, 127, 127, 255), maskGray);

    cv::Mat maskWhite;
    cv::inRange(mat, cv::Scalar(255, 255, 255, 255), cv::Scalar(255, 255, 255, 255), maskWhite);

    mat.setTo(cv::Scalar(255, 255, 255, 255), maskGray);
    mat.setTo(cv::Scalar(127, 127, 127, 255), maskWhite);
    
    return [self trimapWithMatrix:mat dilateSize:size];
}

+ (UIImage *)findContoursAndCreateTrimapImage:(UIImage *)binaryImage dilateSize:(int)size {
    cv::Mat mat;
    UIImageToMat(binaryImage, mat, true);
    
    cv::Mat mask = [self findContoursAndCreateTrimap:mat dilateSize:size];
    
    return MatToUIImage([self maskConvert:mask]);
}

+ (cv::Mat)trimapWithMatrix:(cv::Mat)imgMat dilateSize:(int)size {
    cv::Mat mat = imgMat;
    
    if (mat.channels() == 1) {
        cv::cvtColor(mat, mat, cv::COLOR_GRAY2RGBA);
    }
    else if (mat.channels() == 3) {
        cv::cvtColor(mat, mat, cv::COLOR_RGB2RGBA);
    }
    
    // -----------------------------------
    // Dilate segmentation borders
    int pixels = 2 * size + 1;
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(pixels, pixels), cv::Point(size, size));
    
    cv::dilate(mat, mat, kernel);
    
    // -----------------------------------
    // Gray to White
    // White to Gray
    cv::Mat maskGray;
    cv::inRange(mat, cv::Scalar(127, 127, 127, 255), cv::Scalar(127, 127, 127, 255), maskGray);
    
    cv::Mat maskWhite;
    cv::inRange(mat, cv::Scalar(255, 255, 255, 255), cv::Scalar(255, 255, 255, 255), maskWhite);

    mat.setTo(cv::Scalar(255, 255, 255, 255), maskGray);
    mat.setTo(cv::Scalar(127, 127, 127, 255), maskWhite);
    
    // -----------------------------------
    // Create trimap
    
    int cols = mat.cols;
    int rows = mat.rows;
    cv::Mat mask(rows, cols, CV_8UC1);
    
    // Default set all as background
    mask.setTo(cv::GC_BGD);
    
    // White as foreground
    maskWhite.setTo(0);
    cv::inRange(mat, cv::Scalar(255, 255, 255, 255), cv::Scalar(255, 255, 255, 255), maskWhite);
    mask.setTo(cv::GC_FGD, maskWhite);
    
    // Gray as possible foreground
    maskGray.setTo(0);
    cv::inRange(mat, cv::Scalar(127, 127, 127, 255), cv::Scalar(127, 127, 127, 255), maskGray);
    mask.setTo(cv::GC_PR_FGD, maskGray);
    
    return mask;
}

+ (UIImage *)laplacianEdges:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    // 灰階
    cv::cvtColor(mat, mat, cv::COLOR_RGBA2GRAY);
    
    //先使用3*3内核来降噪
    cv::Mat edge;
    cv::blur(mat, edge, cv::Size(3, 3));
    
    cv::Mat dst;
    cv::Laplacian(edge, dst, CV_8UC4);
    
    return MatToUIImage(dst);
}

+ (UIImage *)sobelEdges:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    // 灰階
    cv::cvtColor(mat, mat, cv::COLOR_RGBA2GRAY);
    
    //先使用3*3内核来降噪
    cv::Mat edge;
    cv::blur(mat, edge, cv::Size(3, 3));
    
    // x方向
    cv::Mat grad_x;
    cv::Sobel(edge, grad_x, CV_8U, 1, 0);
    
    // y方向
    cv::Mat grad_y;
    cv::Sobel(edge, grad_y, CV_8U, 0, 1);

    // 合并的
    cv::addWeighted(grad_x, 0.5, grad_y, 0.5, 0, mat);
    
    return MatToUIImage(mat);
}

+ (UIImage *)cannyEdges:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::Mat dst = [self cannyEdgesWithMatrix:mat];
    return MatToUIImage(dst);
}

+ (UIImage *)cannyEdges:(UIImage *)image threshold:(double)threshold {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::Mat dst = [self cannyEdgesWithMatrix:mat threshold:threshold];
    return MatToUIImage(dst);
}

+ (cv::Mat)cannyEdgesWithMatrix:(cv::Mat)image {
    cv::Mat mat = image;
    
    // 灰階
    cv::Mat gray;
    cv::cvtColor(mat, gray, cv::COLOR_RGBA2GRAY);

    //先使用3*3内核来降噪
    cv::Mat edge;
    cv::blur(gray, edge, cv::Size(3, 3));

    cv::Mat bin;
    double threshold = cv::threshold(edge, bin, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    
    return [self cannyEdgesWithMatrix:image threshold:(threshold / 3.0)];
}

+ (cv::Mat)cannyEdgesWithMatrix:(cv::Mat)image threshold:(double)threshold {
    cv::Mat mat = image;
    
    // 灰階
    cv::cvtColor(mat, mat, cv::COLOR_RGBA2GRAY);
    
    //先使用3*3内核来降噪
    cv::Mat edge;
    cv::blur(mat, edge, cv::Size(3, 3));
    
    cv::Mat dst;
    cv::Canny(edge, dst, threshold, threshold * 3);
//    cv::Canny(edge, dst, 40, 120);
    
    return dst;
}

+ (UIImage *)drawContours:(UIImage *)edgeImage {
    cv::Mat mat;
    UIImageToMat(edgeImage, mat, true);
    
    // 灰階
//    cv::cvtColor(mat, mat, cv::COLOR_RGBA2GRAY);
    
    // 找輪廓
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(mat, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
    
    cv::Mat black(edgeImage.size.height, edgeImage.size.width, CV_8UC4, cv::Scalar(0, 0, 0, 255));
    cv::drawContours(black, contours, -1, cv::Scalar(255, 255, 255, 255));

    return MatToUIImage(black);
}

+ (UIImage *)edgeDetection:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::cvtColor(mat, mat, cv::COLOR_RGBA2GRAY);
    
    mat.convertTo(mat, CV_32F);
    
    cv::Mat kernel = (cv::Mat_<float>(3, 3) << -1, -1, -1, -1, 8, -1, -1, -1, -1);
    
    cv::Mat dst;
    cv::filter2D(mat, dst, -1, kernel, cv::Point(-1, -1), -0.5);

    dst.convertTo(dst, CV_8UC4);
    
    return MatToUIImage(dst);
}

+ (UIImage *)sharpness:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::Mat dst = [self sharpnessWithMatrix:mat];
    return MatToUIImage(dst);
}

+ (UIImage *)sharpness2:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat, true);
    
    cv::Mat frame = [self sharpness2WithMatrix:mat];
    return MatToUIImage(frame);
}

+ (cv::Mat)sharpnessWithMatrix:(cv::Mat)mat {
    mat.convertTo(mat, CV_32F);
        
    // 銳化
//    double vector[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};
//    double vector[] = {-1, -1, -1, -1, 9, -1, -1, -1, -1};
    cv::Mat kernel = (cv::Mat_<float>(3, 3) << -1, -1, -1, -1, 9, -1, -1, -1, -1);
    
    cv::Mat dst;
    cv::filter2D(mat, dst, -1, kernel, cv::Point(-1, -1), 0.0);

    dst.convertTo(dst, CV_8UC4);
    
    return dst;
}

+ (cv::Mat)sharpness2WithMatrix:(cv::Mat)mat {
    cv::Mat frame;
    cv::GaussianBlur(mat, frame, cv::Size(0, 0), 3);
    cv::addWeighted(mat, 1.5, frame, -0.5, 0, frame);
    
    return frame;
}

@end
