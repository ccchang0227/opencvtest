//
//  ViewController.swift
//  OpencvTest
//
//  Created by realtouch's MBP 2019 on 2020/6/15.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

import UIKit
import MBProgressHUD

enum LibType {
    case coreML_1
    case coreML_2
    case tensorflow
}

enum SegmentedState: Int {
    case original = 0
    case segmentation = 1
    case trimap = 2
    case grabCut = 3
    case complete = 4
    case grayScale = 5
    case canny = 6
    case binary = 7
    case sharpness = 8
}

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    static let imgName = "test.jpg"
//    static let imgName = "me1.jpg"
//    static let imgName = "me2.jpg"
//    static let imgName = "us_visa.jpg"
    static let imgName = "imgpsh_fullsize_anim.jpeg"
    
    fileprivate var libType: LibType = .coreML_2
    
    fileprivate var originalImage: UIImage?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var spendTimeLabel: UILabel!

    /// Image segmentator instance that runs image segmentation.
    private var imageSegmentator: ImageSegmentator?
    /// Image segmentation result.
    private var segmentationResult: SegmentationResult?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        var previewImage = UIImage(named: ViewController.imgName)!
        if max(previewImage.size.width, previewImage.size.height) > 1200 {
            var w = previewImage.size.width
            var h = previewImage.size.height
            if previewImage.size.width > previewImage.size.height {
                w = 1200
                h = 1200 * (previewImage.size.height / previewImage.size.width)
            }
            else {
                w = 1200 * (previewImage.size.width / previewImage.size.height)
                h = 1200
            }
            previewImage = previewImage.resize(size: CGSize(width: w, height: h))!
        }
        self.imageView.image = previewImage
        self.originalImage = previewImage
        
        self.imageView2.image = nil
        self.imageView2.isHidden = true
        self.spendTimeLabel.text = nil
        
        guard self.libType == .tensorflow else {
            return
        }
        
        // Initialize an image segmentator instance.
        ImageSegmentator.newInstance { result in
            switch result {
            case let .success(segmentator):
                // Store the initialized instance for use.
                self.imageSegmentator = segmentator
            
                if let originalImage = self.originalImage {
                    let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.runSegmentation(originalImage) {
                        hud.hide(animated: true)
                    }
                }
                
            case .failure(_):
                print("Failed to initialize.")
            }
        }
        
    }
    
    // MARK: -
    
    fileprivate func perform(state: SegmentedState) {
        guard let image = self.originalImage else {
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)

        let start = Date().timeIntervalSinceReferenceDate
        
        DispatchQueue.global().async {
            var previewImage = image
            
            switch state {
            case .segmentation:
                var mask: UIImage
                switch self.libType {
                case .coreML_1:
                    mask = previewImage.segmentation()
                case .coreML_2:
                    mask = previewImage.segmentation2()
                default:
                    guard let result = self.segmentationResult else {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        return
                    }
                    mask = result.resultImage
                }
                
                mask = ImageConverter.binaryImage(mask)

                previewImage = ImageConverter.convertColor(mask,
                                                           from: UIColor.white,
                                                           to: UIColor.clear)
                
            case .trimap:
                var mask: UIImage
                switch self.libType {
                case .coreML_1:
                    mask = previewImage.segmentation()
                case .coreML_2:
                    mask = previewImage.segmentation2()
                default:
                    guard let result = self.segmentationResult else {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        return
                    }
                    mask = result.resultImage
                }
                
                mask = ImageConverter.binaryImage(mask)
                previewImage = ImageConverter.findContoursAndCreateTrimapImage(mask, dilateSize: kDefaultDilateSize)
                
                previewImage = ImageConverter.convertColor(previewImage,
                                                           from: ImageConverter.grabCutFgdColor(),
                                                           to: UIColor.clear)
                previewImage = ImageConverter.convertColor(previewImage,
                                                           from: ImageConverter.grabCutPrFgdColor(),
                                                           to: ImageConverter.grabCutPrFgdColor()!.withAlphaComponent(0.5))
                
            case .grabCut:
                var mask: UIImage
                switch self.libType {
                case .coreML_1:
                    mask = previewImage.segmentation()
                case .coreML_2:
                    mask = previewImage.segmentation2()
                default:
                    guard let result = self.segmentationResult else {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        return
                    }
                    mask = result.resultImage
                }
                
                mask = ImageConverter.binaryImage(mask)
                previewImage = ImageConverter.grabCut(previewImage, mask: mask)
                
                previewImage = ImageConverter.convertColor(previewImage,
                                                           from: ImageConverter.grabCutFgdColor(),
                                                           to: UIColor.clear)
                previewImage = ImageConverter.convertColor(previewImage,
                                                           from: ImageConverter.grabCutPrFgdColor(),
                                                           to:  UIColor(white: 1.0, alpha: 0.5))
                previewImage = ImageConverter.convertColor(previewImage,
                                                           from: ImageConverter.grabCutPrBgdColor(),
                                                           to: UIColor(white: 0.0, alpha: 0.8))
                
            case .complete:
                var mask: UIImage
                switch self.libType {
                case .coreML_1:
                    mask = previewImage.segmentation()
                case .coreML_2:
                    mask = previewImage.segmentation2()
                default:
                    guard let result = self.segmentationResult else {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        return
                    }
                    mask = result.resultImage
                }
                
                mask = ImageConverter.binaryImage(mask)
                previewImage = ImageConverter.grabCut(previewImage, mask: mask)
                
                previewImage = ImageConverter.convertColor(previewImage,
                                                           from: ImageConverter.grabCutFgdColor(),
                                                           to: UIColor.clear)
                previewImage = ImageConverter.convertColor(previewImage,
                                                           from: ImageConverter.grabCutPrFgdColor(),
                                                           to: UIColor.clear)
                
            case .grayScale:
                previewImage = ImageConverter.grayScale(previewImage)
                
            case .canny:
                previewImage = ImageConverter.cannyEdges(previewImage)
                
            case .binary:
                previewImage = ImageConverter.binaryImage(previewImage)
                
            case .sharpness:
                previewImage = ImageConverter.sharpness(previewImage)
                
            default:
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                return
            }
            
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)

                self.imageView2.image = previewImage
                self.imageView2.isHidden = false
                self.imageView2.alpha = 1;
                
                let end = Date().timeIntervalSinceReferenceDate
                let diff = end - start
                print("spend time: \(String(format: "%.2f", diff)) seconds")
                self.spendTimeLabel.text = "spend time: \(String(format: "%.2f", diff)) seconds"
                
            }
        }
    }
    
    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        guard let state = SegmentedState(rawValue: sender.selectedSegmentIndex) else {
            return
        }
        
        switch state {
        case .original:
            self.imageView2.image = nil
            self.imageView2.isHidden = true

            self.spendTimeLabel.text = nil
            
        default:
            self.perform(state: state)
        }
        
    }
    
    @IBAction func cameraButtonTapped(_ sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            self.albumButtonTapped(sender)
            return
        }
        
        let imgPickerCtrl = UIImagePickerController()
        imgPickerCtrl.sourceType = .camera
//        imgPickerCtrl.allowsEditing = true
        imgPickerCtrl.delegate = self
        
        imgPickerCtrl.modalPresentationStyle = .fullScreen
        
        self.present(imgPickerCtrl, animated: true, completion: nil)
    }
    
    @IBAction func albumButtonTapped(_ sender: UIButton) {
        let imgPickerCtrl = UIImagePickerController()
//        imgPickerCtrl.allowsEditing = true
        imgPickerCtrl.delegate = self
        
        imgPickerCtrl.modalPresentationStyle = .fullScreen
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverPresentationController = imgPickerCtrl.popoverPresentationController {
                popoverPresentationController.canOverlapSourceViewRect = true
                popoverPresentationController.permittedArrowDirections = .any
                
                popoverPresentationController.sourceRect = sender.bounds
                popoverPresentationController.sourceView = sender
            }
        }
        
        self.present(imgPickerCtrl, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let originalImage = info[.originalImage] as! UIImage
        if picker.sourceType == .camera {
            UIImageWriteToSavedPhotosAlbum(originalImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        
        if max(originalImage.size.width, originalImage.size.height) > 1200 {
            var w = originalImage.size.width
            var h = originalImage.size.height
            if originalImage.size.width > originalImage.size.height {
                w = 1200
                h = 1200 * (originalImage.size.height / originalImage.size.width)
            }
            else {
                w = 1200 * (originalImage.size.width / originalImage.size.height)
                h = 1200
            }
            self.originalImage = originalImage.resize(size: CGSize(width: w, height: h))
        }
        else {
            // rotate to orientation-up
            let w = originalImage.size.width
            let h = originalImage.size.height
            self.originalImage = originalImage.resize(size: CGSize(width: w, height: h))
        }
        
        self.imageView.image = self.originalImage
        self.imageView2.image = nil
        self.imageView2.isHidden = true
        self.segmentedControl.selectedSegmentIndex = 0
        self.spendTimeLabel.text = nil
        
        guard self.libType == .tensorflow else {
            picker.dismiss(animated: false, completion: nil)
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.runSegmentation(self.originalImage!) {
            hud.hide(animated: true)
        }
        
        picker.dismiss(animated: false, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
    
    @objc
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print("\(error)")
        }
    }
    
}

// MARK: - Image Segmentation

extension ViewController {
    
    /// Run image segmentation on the given image, and show result on screen.
    ///  - Parameter image: The target image for segmentation.
    func runSegmentation(_ image: UIImage, completion: (() -> Void)?) {
        // Rotate target image to .up orientation to avoid potential orientation misalignment.
        guard let targetImage = image.transformOrientationToUp() else {
            completion?()
            return
        }

        // Make sure that image segmentator is initialized.
        guard self.imageSegmentator != nil else {
            completion?()
            return
        }

        // Run image segmentation.
        self.imageSegmentator?.runSegmentation(targetImage) { result in
            // Show the segmentation result on screen
            switch result {
            case .success(let segmentationResult):
                self.segmentationResult = segmentationResult
                
            case .failure(let error):
                print(error)
            }
            
            completion?()
        }
    }

}


class NoSwipeSegmentedControl: UISegmentedControl {
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
