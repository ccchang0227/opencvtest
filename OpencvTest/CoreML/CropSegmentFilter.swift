//
//  CropSegmentFilter.swift
//  deeplab-ios
//
//  Created by realtouch's MBP 2019 on 2020/6/3.
//  Copyright © 2020 xyh. All rights reserved.
//

import Foundation
import CoreImage

class CropSegmentFilter : CIFilter {
    
    private let kernel: CIColorKernel
    var inputImage: CIImage?
    var maskImage: CIImage?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    override init() {
        let kernelStr = """
            kernel vec4 gray(__sample source, __sample mask) {
                float maskValue = mask.r;
                if(maskValue == 0.0){
                   return vec4(0.0, 0.0, 0.0, 0.0);
                }
                return vec4(source.rgb,1.0);
            }
        """
        let kernels = CIColorKernel.makeKernels(source:kernelStr)!
        kernel = kernels[0] as! CIColorKernel
        super.init()
    }
    
    override var outputImage: CIImage? {
        guard let inputImage = inputImage,let maskImage = maskImage else {return nil}
 
        let scale = inputImage.extent.width / maskImage.extent.width
        let suitableMaskImg = maskImage.transformed(by: CGAffineTransform(scaleX: scale, y: scale))
        return kernel.apply(extent: inputImage.extent, arguments:  [inputImage,suitableMaskImg])
    }
    
}
