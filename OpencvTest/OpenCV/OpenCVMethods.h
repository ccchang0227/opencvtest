//
//  OpenCVMethods.h
//  OpencvTest
//
//  Created by realtouch's MBP 2019 on 2020/6/15.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kDefaultDilateSize 20

@interface ImageConverter : NSObject

/// foreground
+ (UIColor *)grabCutFgdColor;
/// background
+ (UIColor *)grabCutBgdColor;
/// possible foreground
+ (UIColor *)grabCutPrFgdColor;
/// possible background
+ (UIColor *)grabCutPrBgdColor;

+ (UIImage *)convertColor:(UIImage *)image from:(UIColor *)fromColor to:(UIColor *)toColor;

+ (UIImage *)grayScaleImage:(UIImage *)image;
+ (UIImage *)binaryImage:(UIImage *)image;

+ (UIImage *)grabCut:(UIImage *)image foregroundBound:(CGRect)bounds;
+ (UIImage *)grabCut:(UIImage *)image mask:(UIImage *)mask;

+ (UIImage *)findContoursAndCreateTrimapImage:(UIImage *)binaryImage dilateSize:(int)size;

+ (UIImage *)laplacianEdges:(UIImage *)image;
+ (UIImage *)sobelEdges:(UIImage *)image;
+ (UIImage *)cannyEdges:(UIImage *)image;
+ (UIImage *)cannyEdges:(UIImage *)image threshold:(double)threshold;

+ (UIImage *)drawContours:(UIImage *)edgeImage;

+ (UIImage *)edgeDetection:(UIImage *)image;
+ (UIImage *)sharpness:(UIImage *)image;
+ (UIImage *)sharpness2:(UIImage *)image;

@end
