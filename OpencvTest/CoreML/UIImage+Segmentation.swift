

import UIKit
import VideoToolbox
import ImageIO

struct ARGB {
    var alpha: UInt8 = 255
    var red: UInt8 = 0
    var green: UInt8 = 0
    var blue: UInt8 = 0
}

extension UIImage {
    
    subscript (x: Int, y: Int) -> UIColor? {
        if x < 0 || x > Int(size.width) || y < 0 || y > Int(size.height) {
            return nil
        }

        let provider = self.cgImage!.dataProvider
        let providerData = provider!.data
        let data = CFDataGetBytePtr(providerData)

        let numberOfComponents = 4
        let pixelData = ((Int(size.width) * y) + x) * numberOfComponents
        
        let a = CGFloat(data![pixelData]) / 255.0
        let r = CGFloat(data![pixelData + 1]) / 255.0
        let g = CGFloat(data![pixelData + 2]) / 255.0
        let b = CGFloat(data![pixelData + 3]) / 255.0

        return UIColor(red: r, green: g, blue: b, alpha: a)
    }

    public func ciEdges() -> UIImage {
        guard let cgImage = self.cgImage else {
            return self
        }
        
        let ciImage = CIImage(cgImage: cgImage)
        
        guard let gray = CIFilter(name: "CIPhotoEffectMono") else {
            return self
        }
        gray.setValue(ciImage, forKey: kCIInputImageKey)
        var output = gray.outputImage!
        
        guard let noiseReduce = CIFilter(name: "CINoiseReduction") else {
            return self
        }
        noiseReduce.setValue(output, forKey: kCIInputImageKey)
        output = noiseReduce.outputImage!
        
        guard let edges = CIFilter(name: "CIEdges") else {
            return self
        }
        edges.setValue(output, forKey: kCIInputImageKey)
        edges.setValue(1.0, forKey: kCIInputIntensityKey)
        output = edges.outputImage!
        
        let ciContext = CIContext(options: nil)
        guard let cgImage2 = ciContext.createCGImage(output, from: ciImage.extent) else {
            return self
        }
        
        return UIImage(cgImage: cgImage2)
    }
    
    func ciEdges2() -> UIImage {
        guard let cgImage = self.cgImage else {
            return self
        }
        let ciImage = CIImage(cgImage: cgImage)
        
        guard let noiseReduce = CIFilter(name: "CINoiseReduction") else {
            return self
        }
        noiseReduce.setValue(ciImage, forKey: kCIInputImageKey)
        var output = noiseReduce.outputImage!
        
        guard let edges = CIFilter(name: "CIConvolution3X3") else {
            return self
        }
        edges.setValue(output, forKey: kCIInputImageKey)
        let values: [CGFloat] = [-1, -1, -1, -1, 8, -1, -1, -1, -1]
        let vector = CIVector(values: values, count: 9)
        edges.setValue(vector, forKey: kCIInputWeightsKey)
        edges.setValue(-0.5, forKey: kCIInputBiasKey)
        output = edges.outputImage!
        
        guard let clamp = CIFilter(name: "CIColorClamp") else {
            return self
        }
        clamp.setValue(output, forKey: kCIInputImageKey)
        clamp.setValue(CIVector(x: 0, y: 0, z: 0, w: 1), forKey: "inputMinComponents")
        clamp.setValue(CIVector(x: 1, y: 1, z: 1, w: 1), forKey: "inputMaxComponents")
        output = clamp.outputImage!
        
        let ciContext = CIContext(options: nil)
        guard let cgImage2 = ciContext.createCGImage(output, from: ciImage.extent) else {
            return self
        }
        
        return UIImage(cgImage: cgImage2)
    }
    
    func ciSharpness() -> UIImage {
        guard let cgImage = self.cgImage else {
            return self
        }
        let ciImage = CIImage(cgImage: cgImage)
        
        guard let noiseReduce = CIFilter(name: "CINoiseReduction") else {
           return self
       }
       noiseReduce.setValue(ciImage, forKey: kCIInputImageKey)
       var output = noiseReduce.outputImage!
        
        guard let convolution = CIFilter(name: "CIConvolution3X3") else {
            return self
        }
        convolution.setValue(output, forKey: kCIInputImageKey)
//        let values: [CGFloat] = [0, -1, 0, -1, 5, -1, 0, -1, 0]
        let values: [CGFloat] = [-1, -1, -1, -1, 9, -1, -1, -1, -1]
        let vector = CIVector(values: values, count: 9)
        convolution.setValue(vector, forKey: kCIInputWeightsKey)
        convolution.setValue(0, forKey: kCIInputBiasKey)
        output = convolution.outputImage!
        
        let ciContext = CIContext(options: nil)
        guard let cgImage2 = ciContext.createCGImage(output, from: ciImage.extent) else {
            return self
        }
        
        return UIImage(cgImage: cgImage2)
    }

    public func segmentation() -> UIImage {
        guard let cgImage = self.coarseSegmentation() else {
            return self
        }
//        let outputWidth:CGFloat = 500.0
//        let outputSize = CGSize(width: outputWidth, height: outputWidth * (self.size.height / self.size.width))
//        let resizeImg = UIImage(cgImage: cgImage).resize(size: outputSize)!
        let resizeImg = UIImage(cgImage: cgImage).resize(size: CGSize(width: self.size.width, height: self.size.height))!
        return resizeImg.smooth()
    }
    
    public func segmentation2() -> UIImage {
        guard let cgImage = self.coarseSegmentation2() else {
            return self
        }
        let resizeImg = UIImage(cgImage: cgImage).resize(size: CGSize(width: self.size.width, height: self.size.height))!
        return resizeImg.smooth()
    }
    
    public func smooth() -> UIImage {
        guard let cgImage = self.cgImage else {
            return self
        }
        let ciImg = CIImage(cgImage: cgImage)
        let smoothFilter = SmoothFilter.init()
        smoothFilter.inputImage = ciImg
        let outputImage = smoothFilter.outputImage!
        
        let ciContext = CIContext(options: nil)
        guard let cgImage2 = ciContext.createCGImage(outputImage, from: ciImg.extent) else {
            return self
        }
        return UIImage(cgImage: cgImage2)
    }
    
    func pixelData() -> [ARGB] {
        let size = self.size
        let dataSize = size.width * size.height * 4
        var pixelData = [UInt8](repeating: 0, count: Int(dataSize))
        let context = CGContext(data: &pixelData,
                                width: Int(size.width),
                                height: Int(size.height),
                                bitsPerComponent: 8,
                                bytesPerRow: 4 * Int(size.width),
                                space: CGColorSpaceCreateDeviceRGB(),
                                bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue + CGBitmapInfo.byteOrder32Big.rawValue)
        guard let cgImage = self.cgImage else { return [] }
        context?.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))

        var pixels = [ARGB]()
        for i in stride(from: 0, to: dataSize, by: 4) {
            var p = ARGB()
            p.alpha = pixelData[Int(i)]
            p.red = pixelData[Int(i + 1)]
            p.green = pixelData[Int(i + 2)]
            p.blue = pixelData[Int(i + 3)]
            pixels.append(p)
        }
        
        return pixels
    }
    
    fileprivate func segmentationAndLightBorder() -> CGImage? {
        guard var cgImage = self.coarseSegmentation() else {
            return nil
        }
//        let outputWidth:CGFloat = 500.0
//        let outputSize = CGSize(width: outputWidth, height: outputWidth * (self.size.height / self.size.width))
//        let resizeImg = UIImage(cgImage: cgImage).resize(size: outputSize)!
        let resizeImg = UIImage(cgImage: cgImage).resize(size: CGSize(width: self.size.width, height: self.size.height))!
        let ciImg = CIImage(cgImage: resizeImg.cgImage!)
        let smoothFilter = SmoothFilter.init()
        smoothFilter.inputImage = ciImg

        let outputImage = smoothFilter.outputImage!
        let ciContext = CIContext(options: nil)
        cgImage = ciContext.createCGImage(outputImage, from: ciImg.extent)!
        
        let image = UIImage(cgImage: cgImage)
        let w = Int(image.size.width)
        let h = Int(image.size.height)
        
        let data = image.pixelData()
        guard data.count != 0 else {
            return cgImage
        }
        
        var tmpPixels = [ARGB](repeating: ARGB(), count: data.count)
        for y in 0..<h {
            for x in 0..<w {
                let p = data[(y * w + x)]
                
                var isBorder = false
                if p.red >= 128 && p.green >= 128 && p.blue >= 128 {
                    
                    // 檢查九宮格的像素點內是否有顏色劇烈變化的點（有的話就是邊緣）
                    for i in 0..<9 {
                        if i == 4 {
                            continue
                        }

                        var ax = i % 3
                        var by = (i - ax) / 3
                        ax = x + ax - 1
                        by = y + by - 1
                        guard ax >= 0 && ax < w && by >= 0 && by < h else {
                            continue
                        }
                        
                        let index = by * w + ax
                        guard index >= 0 && index < data.count else {
                            break
                        }
                        let p2 = data[index]
                        if p2.red < 128 && p2.green < 128 && p2.blue < 128 {
                            isBorder = true
                            break
                        }
                    }
                }
                
                if isBorder {
                    // 邊緣：白色
                    tmpPixels[(y * w + x)] = ARGB(alpha: 255, red: 255, green: 255, blue: 255)
//                    tmpPixels[(y * w + x)] = ARGB(alpha: 255, red: 255, green: 0, blue: 0)
                }
                else {
                    if p.red >= 128 && p.green >= 128 && p.blue >= 128 {
                        // 前景：灰色
//                        tmpPixels[(y * w + x)] = ARGB(alpha: 255, red: 255, green: 255, blue: 255)
                        tmpPixels[(y * w + x)] = ARGB(alpha: 255, red: 127, green: 127, blue: 127)
                    }
                    else {
                        // 背景：黑色
                        tmpPixels[(y * w + x)] = ARGB(alpha: 255, red: 0, green: 0, blue: 0)
                    }
                }
            }
        }
        
        let tmp = tmpPixels.flatMap { [$0.alpha, $0.red, $0.green, $0.blue] }
        let bytesPerComponent = MemoryLayout<UInt8>.size
        let bytesPerPixel = bytesPerComponent * 4
        let alphaPremultipliedLast = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)
        let provider: CGDataProvider = CGDataProvider(data: Data(tmp) as CFData)!
        cgImage = CGImage(
            width: w,
            height: h,
            bitsPerComponent: bytesPerComponent * 8,
            bitsPerPixel: bytesPerPixel * 8,
            bytesPerRow: bytesPerPixel * w,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: [.byteOrder32Big, alphaPremultipliedLast],
            provider: provider,
            decode: nil,
            shouldInterpolate: false,
            intent: CGColorRenderingIntent.defaultIntent
            )!
        return cgImage
    }
    
    public func coarseSegmentation() -> CGImage? {
        let deeplab = Deeplab.init()
        //input size 513*513
        let pixBuf = self.pixelBuffer(width: 513, height: 513)
        
        guard let output = try? deeplab.prediction(ImageTensor__0: pixBuf!) else {
            return nil
        }
        
        let shape = output.ResizeBilinear_2__0.shape
        let (d,w,h) = (Int(truncating: shape[0]), Int(truncating: shape[1]), Int(truncating: shape[2]))
        let pageSize = w*h
        var res:Array<Int> = []
        var pageIndexs:Array<Int> = []
        for i in 0..<d {
            pageIndexs.append(pageSize * i)
        }
 
        func argmax(arr:Array<Int>) -> Int{
            precondition(arr.count > 0)
            var maxValue = arr[0]
            var maxValueIndex = 0
            for i in 1..<arr.count {
                if arr[i] > maxValue {
                    maxValue = arr[i]
                    maxValueIndex = i
                }
            }
            return maxValueIndex
        }
        
        for i in 0..<w {
            for j in 0..<h {
                var itemArr:Array<Int> = []
                let pageOffset = i * w + j
                for k in 0..<d {
                    let padding = pageIndexs[k]
                    itemArr.append(Int(truncating: output.ResizeBilinear_2__0[padding + pageOffset]))
                }
                /*
                types map  [
                    'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus',
                    'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike',
                    'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tv'
                    ]
                 */
                let type = argmax(arr: itemArr)
                res.append(type)
            }
        }
        
        let bytesPerComponent = MemoryLayout<UInt8>.size
        let bytesPerPixel = bytesPerComponent * 4
        let length = pageSize * bytesPerPixel
        var data = Data(count: length)
        
        /*
        This reserved only [cat,dog,person]
        */
        let reserve = 1...20//[8,12,15]
        var index = 0
        for pix in res {
//            let v: UInt8 = reserve.contains(pix) ? 255 : 0
//            for _ in 0...3 {
//                pointer.pointee = v
//                pointer += 1
//            }
            
            let r: UInt8 = reserve.contains(pix) ? 255 : 0
            let g: UInt8 = reserve.contains(pix) ? 255 : 0
            let b: UInt8 = reserve.contains(pix) ? 255 : 0
            let a: UInt8 = 255
            data[index] = a
            index += 1
            data[index] = r
            index += 1
            data[index] = g
            index += 1
            data[index] = b
            index += 1
        }
        
        let alphaPremultipliedLast = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)
        let provider: CGDataProvider = CGDataProvider(data: data as CFData)!
        let cgimg = CGImage(
            width: w,
            height: h,
            bitsPerComponent: bytesPerComponent * 8,
            bitsPerPixel: bytesPerPixel * 8,
            bytesPerRow: bytesPerPixel * w,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: [.byteOrder32Big, alphaPremultipliedLast],
            provider: provider,
            decode: nil,
            shouldInterpolate: false,
            intent: CGColorRenderingIntent.defaultIntent
            )
        return cgimg
    }
    
    public func coarseSegmentation2() -> CGImage? {
        let deeplab = DeepLabV3.init()
        //input size 513*513
        let pixBuf = self.pixelBuffer(width: 513, height: 513)
        
        guard let output = try? deeplab.prediction(image: pixBuf!) else {
            return nil
        }
        
        let shape = output.semanticPredictions.shape
        let (w, h) = (Int(truncating: shape[0]), Int(truncating: shape[1]))
        let pageSize = w * h
        
        var data = [ARGB](repeating: ARGB(alpha: 255, red: 0, green: 0, blue: 0), count: pageSize)
        for i in 0..<h {
            for j in 0..<w {
                let pageOffset = i * w + j
                let value = Int(truncating: output.semanticPredictions[pageOffset])
                
                /*
                types map  [
                    'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus',
                    'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike',
                    'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tv'
                    ]
                 */
                switch value {
                case 0:
                    // background
                    data[pageOffset] = ARGB(alpha: 255, red: 0, green: 0, blue: 0)
                default:
                    // else
                    data[pageOffset] = ARGB(alpha: 255, red: 255, green: 255, blue: 255)
                }
            }
        }
        
        let tmp = data.flatMap { [$0.alpha, $0.red, $0.green, $0.blue] }
        let bytesPerComponent = MemoryLayout<UInt8>.size
        let bytesPerPixel = bytesPerComponent * 4
        let alphaPremultipliedLast = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)
        let provider: CGDataProvider = CGDataProvider(data: Data(tmp) as CFData)!
        let cgImage = CGImage(
            width: w,
            height: h,
            bitsPerComponent: bytesPerComponent * 8,
            bitsPerPixel: bytesPerPixel * 8,
            bytesPerRow: bytesPerPixel * w,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: [.byteOrder32Big, alphaPremultipliedLast],
            provider: provider,
            decode: nil,
            shouldInterpolate: false,
            intent: CGColorRenderingIntent.defaultIntent
            )!
        return cgImage
    }
    
}

extension UIImage {

  public func pixelBuffer(width: Int, height: Int) -> CVPixelBuffer? {
    return pixelBuffer(width: width, height: height,
                       pixelFormatType: kCVPixelFormatType_32ARGB,
                       colorSpace: CGColorSpaceCreateDeviceRGB(),
                       alphaInfo: .noneSkipFirst)
  }
 
  func pixelBuffer(width: Int, height: Int, pixelFormatType: OSType,
                   colorSpace: CGColorSpace, alphaInfo: CGImageAlphaInfo) -> CVPixelBuffer? {
    var maybePixelBuffer: CVPixelBuffer?
    let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue,
                 kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue]
    let status = CVPixelBufferCreate(kCFAllocatorDefault,
                                     width,
                                     height,
                                     pixelFormatType,
                                     attrs as CFDictionary,
                                     &maybePixelBuffer)

    guard status == kCVReturnSuccess, let pixelBuffer = maybePixelBuffer else {
      return nil
    }

    CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
    let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer)

    guard let context = CGContext(data: pixelData,
                                  width: width,
                                  height: height,
                                  bitsPerComponent: 8,
                                  bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer),
                                  space: colorSpace,
                                  bitmapInfo: alphaInfo.rawValue)
    else {
      return nil
    }

    UIGraphicsPushContext(context)
    context.translateBy(x: 0, y: CGFloat(height))
    context.scaleBy(x: 1, y: -1)
    self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
    UIGraphicsPopContext()
    CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
    return pixelBuffer
  }
}

extension UIImage {
    
    func resize(size: CGSize!) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in:rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
 
}


fileprivate class SmoothFilter : CIFilter {
    
    private let kernel: CIColorKernel
    var inputImage: CIImage?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init() {
        let kernelStr = """
            kernel vec4 myColor(__sample source) {
                vec3 maskValue = smoothstep(0.3, 0.5, source.rgb);
                //float maskValue = smoothstep(0.3, 0.5, source.r);
                return vec4(maskValue.rgb, source.a);
            }
        """
        let kernels = CIColorKernel.makeKernels(source:kernelStr)!
        kernel = kernels[0] as! CIColorKernel
        super.init()
    }
    
    override var outputImage: CIImage? {
        guard let inputImage = inputImage else {return nil}
        let blurFilter = CIFilter.init(name: "CIGaussianBlur")!
        blurFilter.setDefaults()
        blurFilter.setValue(inputImage.extent.width / 90.0, forKey: kCIInputRadiusKey)
        blurFilter.setValue(inputImage, forKey: kCIInputImageKey)
        let bluredImage = blurFilter.value(forKey:kCIOutputImageKey) as! CIImage
        return kernel.apply(extent: bluredImage.extent, arguments: [bluredImage])
    }
}
